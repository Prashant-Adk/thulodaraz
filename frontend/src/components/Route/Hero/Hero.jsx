import React from 'react'
import styles from '../../../styles/styles'
import { Link } from 'react-router-dom'

const Hero = () => {
  return (
    <div className={`relative max-h-[70vh] 800px:max-h-[80vh] w-full bg-no-repeat ${styles.noramlFlex}`} style={{
        backgroundImage: "https://images.unsplash.com/photo-1563286094-6e420626b6f3?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1750&q=80",
    }}
    >
        <div className={`${styles.section} w-[100%] 800px:w-[70%]`}>
            <h1 className={`text-[35px] leading-[1.2] 800px:text-[50px] text-gray-500 font-[600] capitalize font-Orbitron mt-20`}>
                Best Collection for <br/> home Decoration
            </h1>
            <p className="pt-5 text-[16px] font-[400] text-black">
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Repellendus eius magnam amet qui voluptate ab adipisci deserunt eveniet odio, itaque, fugiat id cum totam ut! Unde fugiat corporis voluptatum amet. <br/>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quo soluta harum maiores assumenda, voluptas maxime, dolorem eos iusto rem, saepe laborum nisi aliquid! Dolorem, corporis obcaecati nesciunt recusandae accusantium facere?
            </p>
            <Link to="/products" className='inline-block'>
                <div className={`${styles.button} mt-5`}>
                    <span className="text-white font-Orbitron text-[18px]">
                        Shop Now</span></div>
            </Link>
        </div>
    </div>
  )
}

export default Hero