const app = require("./app");
const connectDatabase = require("./db/Database");

process.on("uncaughtException", (err) => {
  console.log(`Error:${err.message}`);
  console.log("shutting down the server for handling uncaught exception");
});

if (process.env.NODE_ENV !== "PRODUCTION") {
  require("dotenv").config({
    path: "./config/.env",
  });
}

connectDatabase();

const server = app.listen(process.env.PORT, () => {
  console.log(`server created at port ${process.env.PORT}`);
});

process.on("unhandledRejection", (err) => {
  console.log(`shutting down the server ${err.message}`);
  console.log(`"shutting down the server for unhandle promise rejection`);

  server.close(() => {
    process.exit(1);
  });
});
